#!/usr/bin/env bash

if [ ! -f /home/user/.ssh/authorized_keys ]; then
    su - user -c "echo '$PUBLIC_KEY' >> /home/user/.ssh/authorized_keys"
fi

# Start SSH server
/usr/sbin/sshd -D