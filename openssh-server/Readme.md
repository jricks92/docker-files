Dockerized OpenSSH Server
============

Recently I had to do some testing of some SSH and SFTP scripts. In order to be more efficient, I decided to create a dockerized OpenSSH server. Why wouldn't I just `ssh localhost` to test? The scripts I'm dealing with can recursively delete files. In order to keep things safer and isolated, I decide to create this container.

This is based off this article: https://docs.docker.com/engine/examples/running_ssh_service/

Running the container:
```bash
$ docker run -d -P -e PUBLIC_KEY="<your public SSH key>" --name ssh_server jricks92/openssh
```
Important Flags:

`-d` Runs the container in daemon mode

`-P` Publishes the remote port

`-e PUBLIC_KEY='<your public SSH key>'` Allows you to connect to the server without a password using your SSH key

Connecting to the container:
```bash
$ docker port ssh_server 22
0.0.0.0:32768

$ ssh -p 32768 user@localhost
Welcome to Ubuntu 18.04.3 LTS (GNU/Linux 4.9.184-linuxkit x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

This system has been minimized by removing packages and content that are
not required on a system that users do not log into.

To restore this content, you can run the 'unminimize' command.

user@255ad9a37087:~$
```
By running the `docker port` command, we can see what local port the docker service mapped the container's SSH port to. From there we use the default name of `user` to connect to the server. The full connection string is `ssh -p <Some port number> user@localhost`. When you connect, you should be greeted by the ubuntu welcome header.



