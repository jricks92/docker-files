#!/bin/sh

echo "Setting environment variables..."
/bin/sed -i -e "s/<APC_SHM_SIZE>/$APC_SHM_SIZE/g" /usr/local/etc/php/conf.d/apcu.ini \
       -e "s/<OPCACHE_MEM_SIZE>/$OPCACHE_MEM_SIZE/g" /usr/local/etc/php/conf.d/opcache.ini \
       -e "s/<MEMORY_LIMIT>/$MEMORY_LIMIT/g" /usr/local/bin/occ \
       -e "s/<UPLOAD_MAX_SIZE>/$UPLOAD_MAX_SIZE/g" /etc/nginx/nginx.conf /usr/local/etc/php-fpm.conf \
       -e "s/<MEMORY_LIMIT>/$MEMORY_LIMIT/g" /usr/local/etc/php-fpm.conf

# Put the configuration and apps into volumes
ln -sf /config/config.php /nextcloud/config/config.php &>/dev/null
ln -sf /apps2 /nextcloud &>/dev/null

# Create folder for php sessions if not exists
if [ ! -d /usr/local/etc/php/session ]; then
  mkdir -p /usr/local/etc/php/session;
fi

# Set timezone
echo "Setting timezone..."
ln -fs /usr/share/zoneinfo/${TZ} /etc/localtime
dpkg-reconfigure --frontend noninteractive tzdata

echo "Checking that we have created a user and group for nextcloud..."
getent group $GID >/dev/null || groupadd --gid $GID nextcloud >/dev/null
getent passwd $UID >/dev/null || useradd nextcloud --uid $UID --gid $GID --shell /bin/bash >/dev/null

echo "Updating permissions..."
for dir in /nextcloud /config /apps2 /var/log /usr/local/etc/php /usr/local/etc/php-fpm.conf /etc/nginx /usr/share/nginx/ /usr/local/etc/php/session /tmp /etc/s6.d /run /data; do
  if [ $dir = "/data" ] && [ "${SKIP_FILE_PERMISSIONS}" = "true" ]; then
    echo "Skipping updating permissions for $dir..."
  else
    if eval find $dir ! -user $UID -o ! -group $GID | grep -E '.' -q; then
      echo "Updating permissions in $dir..."
      chown -R $UID:$GID $dir
    else
      echo "Permissions in $dir are correct."
    fi
  fi
done
echo "Done updating permissions."

if [ ! -f /config/config.php ]; then
    # New installation, run the setup
    /usr/local/bin/setup.sh
else
    echo "Checking if we need to upgrade..."
    occ upgrade
fi

echo "Starting main processes."
exec su-exec $UID:$GID /bin/s6-svscan /etc/s6.d
