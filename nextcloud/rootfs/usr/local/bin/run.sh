#!/bin/sh

echo "Setting environment variables..."
/bin/sed -i -e "s/<APC_SHM_SIZE>/$APC_SHM_SIZE/g" /php/conf.d/apcu.ini \
       -e "s/<OPCACHE_MEM_SIZE>/$OPCACHE_MEM_SIZE/g" /php/conf.d/opcache.ini \
       -e "s/<MEMORY_LIMIT>/$MEMORY_LIMIT/g" /usr/local/bin/occ \
       -e "s/<UPLOAD_MAX_SIZE>/$UPLOAD_MAX_SIZE/g" /nginx/conf/nginx.conf /php/etc/php-fpm.conf \
       -e "s/<MEMORY_LIMIT>/$MEMORY_LIMIT/g" /php/etc/php-fpm.conf
# /bin/sed -i -e "s/<CRON_MEMORY_LIMIT>/$CRON_MEMORY_LIMIT/g" /etc/s6.d/cron/run \
#        -e "s/<CRON_PERIOD>/$CRON_PERIOD/g" /etc/s6.d/cron/run

# Put the configuration and apps into volumes
ln -sf /config/config.php /nextcloud/config/config.php &>/dev/null
ln -sf /apps2 /nextcloud &>/dev/null

# Create folder for php sessions if not exists
if [ ! -d /data/session ]; then
  mkdir -p /data/session;
fi

if [ "${SKIP_FILE_PERMISSIONS}" != "true" ]; then
  echo "Updating permissions..."
  for dir in /nextcloud /config /apps2 /var/log /php /nginx /tmp /etc/s6.d /data; do
    if eval find $dir ! -user $UID -o ! -group $GID|grep -E '.' -q; then
      echo "Updating permissions in $dir..."
      chown -R $UID:$GID $dir
    else
      echo "Permissions in $dir are correct."
    fi
  done
  echo "Done updating permissions."
fi

if [ ! -f /config/config.php ]; then
    # New installation, run the setup
    /usr/local/bin/setup.sh
else
    echo "Checking if we need to upgrade..."
    occ upgrade
fi

echo "Starting main processes."
exec su-exec $UID:$GID /bin/s6-svscan /etc/s6.d
