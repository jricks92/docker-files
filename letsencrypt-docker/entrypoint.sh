#!/bin/bash

echo "$(date) - Requesting certificate"

# Build Certbot command
# COMMAND="certbot certonly --standalone"
COMMAND="certbot certonly"

# COMMAND="$COMMAND --text --agree-tos --email $EMAIL --server https://acme-v02.api.letsencrypt.org/directory --rsa-key-size 4096 --verbose --standalone-supported-challenges http-01 --no-eff-email"
COMMAND="$COMMAND --text \
--agree-tos \
--email $EMAIL \
--rsa-key-size 4096 \
--no-eff-email \
--non-interactive \
--expand"

# Check if we're using Route 53
if [ "$AWS_ACCESS_KEY_ID" ] || [ -f "$AWS_SHARED_CREDENTIALS_FILE" ] || [ "$USE_EC2_ROLE" ]; then
    echo "Using Route 53 method.";
    COMMAND="$COMMAND --dns-route53"
    if [ "$ROUTE53_PROPAGATION_SECONDS" ]; then
        COMMAND="$COMMAND --dns-route53-propagation-seconds $ROUTE53_PROPAGATION_SECONDS"
    fi
    # unset the access key variables if we're using the EC2 instance profile
    if [ "$USE_EC2_ROLE" ]; then
        echo "Using EC2 Instance Profile. Cleaning up environment variables."
        unset AWS_ACCESS_KEY_ID
        unset AWS_SECRET_ACCESS_KEY
    fi
else
    echo "Using --webroot method."
    COMMAND="$COMMAND --webroot -w $WEBROOT"
fi
# --standalone-supported-challenges http-01"

echo "We found the following domains. Adding them to the certbot command:"

DOMAINS_LIST=($(echo $DOMAINS | tr "," "\n"))
for DOMAIN in ${DOMAINS_LIST[@]}
do
    # Remove quotes if they're there
    DOMAIN="${DOMAIN%\"}"
    DOMAIN="${DOMAIN#\"}"

    echo "$DOMAIN"
    COMMAND="$COMMAND -d $DOMAIN"
done


# Check if we're forcing a renewal
if [ "$FORCE_RENEWAL" = true ] ; then
    COMMAND="$COMMAND --renew-by-default"
else
    COMMAND="$COMMAND --keep-until-expiring"
fi

while :
do
    echo "--------------------------------------------------------------"
    echo "$(date) - Checking for certificate renewal"
    echo "--------------------------------------------------------------"

    if [ "$DRYRUN" = true ] ; then
        COMMAND="$COMMAND --dry-run"
    else
        # Add the ACME v2 endpoint if we're not doing --dry-run (allows wildcards)
        COMMAND="$COMMAND --server https://acme-v02.api.letsencrypt.org/directory"
    fi

    if [ "$VERBOSE" = true ] ; then
        COMMAND="$COMMAND --verbose"
    fi

    # Run Command
    echo "Running \"$COMMAND\""
    OUTPUT=$(eval $COMMAND)


    if [ "$SERVER_CONTAINER" ]; then
        if  [[ "$OUTPUT" =~ "Certificate not yet due for renewal;" ]]; then
            echo "No new certificate found. $SERVER_CONTAINER will not be restarted.";
        else
            echo "New certificate. Reloading web server configuration container: $SERVER_CONTAINER"
            eval docker restart $SERVER_CONTAINER;
        fi
    fi

    # Sleep for specified period
    echo "Next check for new Let's Encrypt Cert will be in $RENEW_TIME."
    sleep $RENEW_TIME
done;
