#!/usr/bin/env bash

# Save SSH Key
echo "${SSH_KEY}" > ~/.ssh/authorized_keys
# Generate new SSH host keys
ssh-keygen -A
# Start SSHD
/usr/sbin/sshd

echo "VPN Protocol: $PROTOCOL"
echo "VPN Endpoint: $HOSTNAME"
echo "VPN Username: $USERNAME"
echo "Extra Args: $EXTRA_ARGS"

if [ -n "$USERNAME" ] && [ -n "$PASSWORD" ]; then
    echo "Connecting to VPN ($PROTOCOL) at $HOSTNAME..."
    echo "$PASSWORD" | openconnect --protocol="$PROTOCOL" -u "$USERNAME" --passwd-on-stdin "$HOSTNAME" $EXTRA_ARGS
elif [ -n "$USERNAME" ]; then
    openconnect --protocol="$PROTOCOL" -u "$USERNAME" "$HOSTNAME"
else
    echo "Username and Password fields are empty..."
    /usr/bin/env bash
fi