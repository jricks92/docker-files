FROM debian:bullseye-slim

ARG BUILD_CORES

ARG NGINX_VER=1.27.0
ARG PHP_VER=8.2.20
ARG LIBICONV_VERSION=1.17
ARG IMAGEMAGICK_VERSION=7.1.1-34

LABEL description="nginx + php image based on ubuntu CUDA with HTTP/2 and ImageMagick support" \
      maintainer="Jameson Ricks" \
      php_version="PHP v$PHP_VER built from source" \
      nginx_version="nginx v$NGINX_VER built from source"

ARG PHP_MIRROR=http://ch1.php.net
ARG RTMP_MODULE_SRC=https://github.com/mannyamorim/nginx-rtmp-module.git

ARG NGINX_CONF=" \
   --prefix=/usr/share/nginx \
   --conf-path=/etc/nginx/nginx.conf \
   --modules-path=/etc/nginx/modules \
   --sbin-path=/usr/sbin/nginx \
   --http-log-path=/var/log/nginx/access.log \
   --error-log-path=/var/log/nginx/error.log \
   --pid-path=/run/nginx.pid \
   --lock-path=/run/lock/nginx.lock \
   --with-threads \
   --with-file-aio \
   --with-http_ssl_module \
   --with-http_gzip_static_module \
   --with-http_v2_module \
   --with-http_realip_module \
   --without-http_geo_module \
   --without-http_autoindex_module \
   --without-http_split_clients_module \
   --without-http_memcached_module \
   --without-http_empty_gif_module \
   --without-http_browser_module \
   "
   # --with-openssl=/usr/src/libressl-${LIBRESSL_VERSION}"

ARG PHP_CONF=" \
   --with-config-file-path=/usr/local/etc/php \
   --with-config-file-scan-dir=/usr/local/etc/php/conf.d \
   --with-pear \
   --with-pic \
   --disable-short-tags \
   --without-readline \
   --enable-bcmath=shared \
   --enable-fpm \
   --disable-cgi \
   --enable-mysqlnd \
   --enable-mbstring \
   --with-curl \
   --with-libedit \
   # --with-openssl=/usr/src/libressl-${LIBRESSL_VERSION} \
   --with-openssl \
   --with-iconv=/usr/local \
   --enable-gd \
   --with-freetype \
   --with-jpeg \
   --with-webp \
   --disable-gd-jis-conv \
   --with-zlib \
   --enable-sysvsem \
   --enable-sysvshm \
   --enable-sysvmsg"

ARG PHP_EXT_LIST=" \
   bcmath \
   bz2 \
   ctype \
   curl \
   exif \
   fileinfo \
   ftp \
   gd \
   gmp \
   imap \
   intl \
   ldap \
   mbstring \
   mysqli \
   openssl \
   pcntl \
   pdo_mysql \
   pdo_pgsql \
   pdo_sqlite \
   pgsql \
   posix \
   session \
   simplexml \
   sodium \
   sqlite3 \
   xml \
   zip \
   zlib \
   "

ARG CUSTOM_BUILD_PKGS=" \
   fftw-dev \
   libc-client-dev \
   libde265-dev \
   libedit-dev \
   libfontconfig1-dev \
   libfreetype-dev \
   libgd-dev \
   libghc-bz2-dev \
   libgmp-dev \
   libgraphviz-dev \
   libheif-dev \
   libicu-dev \
   libjbig-dev \
   libjbig2dec0-dev \
   libjpeg62-turbo-dev \
   libkrb5-dev \
   libldap2-dev \
   liblzma-dev \
   libmcrypt-dev \
   libmp3lame-dev \
   libonig-dev \
   libopenjp2-7-dev \
   libpng-dev \
   libpng-dev \
   libpq-dev \
   libpq-dev\
   libraw-dev \
   librtmp-dev \
   libsodium-dev \
   libsqlite3-dev \
   libssl-dev \
   libtheora-dev \
   libvorbis-dev \
   libvpx-dev \
   libwebp-dev \
   libx264-dev \
   libxml2-dev \
   libxpm-dev \
   libzip-dev \
   libzstd-dev \
   libmemcached-dev \
   "


ARG CUSTOM_PKGS=" \
   ffmpeg \
   fftw2 \
   fontconfig \
   graphviz \
   libbz2-1.0 \
   libc-client2007e \
   libde265-0 \
   libedit2 \
   libfreetype6 \
   libgd3 \
   libgmp10 \
   libheif1 \
   libicu67 \
   libjbig0 \
   libjbig2dec0 \
   libjpeg62-turbo \
   libldap-2.4-2 \
   liblzma5 \
   libmcrypt4 \
   libmp3lame0 \
   libonig5 \
   libonig5 \
   libopenjp2-7 \
   libpng16-16 \
   libpq5 \
   libraw20 \
   libsodium23 \
   libwebp6 \
   libwebpdemux2 \
   libwebpmux3 \
   libx264-160 \
   libxml2 \
   libxpm4 \
   libzip4 \
   rtmpdump \
   sqlite3 \
   zstd \
   libmemcached11 \
   "

COPY rootfs-debian /

RUN NB_CORES=${BUILD_CORES:-$(nproc)} \
### Packages installation
   && BUILD_DEPS=" \
      libtool-bin \
      build-essential \
      libpcre2-dev \
      zlib1g-dev \
      wget \
      gnupg \
      autoconf \
      gcc \
      g++ \
      linux-libc-dev \
      libcurl4-openssl-dev \
      make \
      pkgconf \
      ca-certificates \
      git \
      ${CUSTOM_BUILD_PKGS}" \
   && apt-get update && apt-get install --no-install-recommends -y \
      ${BUILD_DEPS} \
      s6 \
      procps \
      curl \
      libedit2 \
      libxml2 \
      libcurl4 \
      openssl \
      webp \
      libpcre2-8-0 \
      zlib1g \
      sudo \
      ${CUSTOM_PKGS} \
   && mkdir -p /usr/src \
### su-exec
   && echo "Building and installing su-exec" \
   && curl -o /tmp/su-exec.c https://raw.githubusercontent.com/ncopa/su-exec/master/su-exec.c \
   && gcc -Wall /tmp/su-exec.c -o /usr/local/bin/su-exec \
   && chown root:root /usr/local/bin/su-exec \
   && chmod 0755 /usr/local/bin/su-exec \
## imagemagick installation
   && echo "Building ImageMagick with HEIC support" \
   && wget https://github.com/ImageMagick/ImageMagick/archive/${IMAGEMAGICK_VERSION}.tar.gz -O /tmp/ImageMagick-${IMAGEMAGICK_VERSION}.tar.gz \
   && tar xzf /tmp/ImageMagick-${IMAGEMAGICK_VERSION}.tar.gz -C /usr/src \
   && cd /usr/src/ImageMagick-${IMAGEMAGICK_VERSION} \
   && ./configure --with-heic=yes --prefix=/usr  \
   && make -j ${NB_CORES} && make install \
   && ldconfig /usr/lib/ \
### nginx installation
   && echo "Building NGINX with RTMP support" \
   && wget http://nginx.org/download/nginx-${NGINX_VER}.tar.gz -O /tmp/nginx-${NGINX_VER}.tar.gz \
   && wget http://nginx.org/download/nginx-${NGINX_VER}.tar.gz.asc -O /tmp/nginx-${NGINX_VER}.tar.gz.asc \
   && tar xzf /tmp/nginx-${NGINX_VER}.tar.gz -C /usr/src \
   && git clone ${RTMP_MODULE_SRC} /tmp/nginx-rtmp-module \
   && cd /usr/src/nginx-${NGINX_VER} \
   && ./configure --with-cc-opt="-O3 -fPIE -fstack-protector-strong" --add-module=/tmp/nginx-rtmp-module ${NGINX_CONF} \
   && make -j ${NB_CORES} \
   && make install \
   && mkdir -p /etc/nginx/sites-enabled \
### GNU Libiconv installation
   && echo "Building Libiconv" \
   && wget http://ftp.gnu.org/pub/gnu/libiconv/libiconv-${LIBICONV_VERSION}.tar.gz -O /tmp/libiconv-${LIBICONV_VERSION}.tar.gz \
   && tar xzf /tmp/libiconv-${LIBICONV_VERSION}.tar.gz -C /usr/src \
   && cd /usr/src/libiconv-${LIBICONV_VERSION} \
   && ./configure --prefix=/usr/local \
   && make -j ${NB_CORES} && make install && libtool --finish /usr/local/lib \
### PHP installation
   && echo "Building PHP" \
   && wget ${PHP_MIRROR}/get/php-${PHP_VER}.tar.gz/from/this/mirror -O /tmp/php-${PHP_VER}.tar.gz \
   && wget ${PHP_MIRROR}/get/php-${PHP_VER}.tar.gz.asc/from/this/mirror -O /tmp/php-${PHP_VER}.tar.gz.asc \
   && tar xzf /tmp/php-${PHP_VER}.tar.gz -C /usr/src \
   && mv /usr/src/php-${PHP_VER} /usr/src/php \
   && cd /usr/src/php \
   && ./configure CFLAGS="-fstack-protector-strong -fpic -fpie -O2 -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64" ${PHP_CONF} \
   && make -j ${NB_CORES} \
   && make install \
   && mkdir -p /usr/local/etc/php/conf.d \
### Strip, clean, install modules
   && { find /usr/local/bin /usr/local/sbin -type f -perm +0111 -exec strip --strip-all '{}' + || true; } \
   && make clean \
   && ln -sf /usr/bin/s6* /bin/ \
   && chmod u+x /usr/local/bin/* /etc/s6.d/*/* \
   && chmod u+x /usr/local/bin/docker-php-ext-* \
   && pecl channel-update pecl.php.net \
   && pecl install -f mcrypt \
   && echo "extension=mcrypt.so" > /usr/local/etc/php/conf.d/mcrypt.ini \
   && pecl install -f imagick \
   && echo "extension=imagick.so" > /usr/local/etc/php/conf.d/imagick.ini \
   && pecl install -f apcu \
   && echo "extension=apcu.so" > /usr/local/etc/php/conf.d/apcu.ini \
   && pecl install -f redis \
   && echo "extension=redis.so" > /usr/local/etc/php/conf.d/redis.ini \
   && pecl install -f memcached \
   && echo "extension=memcached.so" > /usr/local/etc/php/conf.d/memcached.ini \
   ## Add Library Path for freetype2 to allow gd to compile successfully
   && export CPATH=/usr/include/freetype2 \
   # configure imap separately
   && docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
   && docker-php-ext-install ${PHP_EXT_LIST} -j ${NB_CORES} \
### Cleanup
   && apt-get remove -y ${BUILD_DEPS} \
   && rm -rf /var/lib/apt/lists/* \
   && apt-get purge -y --auto-remove \
   && apt-get autoremove \
   && apt-get clean \
   && rm -rf /tmp/* /usr/src/* \
   && mkdir -p /var/log/nginx /run /usr/local/etc/php/conf.d /usr/local/etc/php/session /var/log/php