FROM alpine:3

ARG BUILD_CORES

ARG NGINX_VER=1.21.3
ARG PHP_VER=8.0.10
ARG LIBICONV_VERSION=1.16
ARG IMAGEMAGICK_VERSION=7.1.0-6
ARG FFMPEG_VERSION=5.1.2

LABEL description="nginx + php image based on Alpine with HTTP/2 and ImageMagick support" \
      maintainer="Jameson Ricks" \
      php_version="PHP v$PHP_VER built from source" \
      nginx_version="nginx v$NGINX_VER built from source"

ARG PHP_MIRROR=http://ch1.php.net
ARG RTMP_MODULE_SRC=https://github.com/mannyamorim/nginx-rtmp-module.git

ARG NGINX_CONF=" \
   --prefix=/nginx \
   --sbin-path=/usr/local/sbin/nginx \
   --http-log-path=/nginx/logs/access.log \
   --error-log-path=/nginx/logs/error.log \
   --pid-path=/nginx/run/nginx.pid \
   --lock-path=/nginx/run/nginx.lock \
   --with-threads \
   --with-file-aio \
   --with-http_ssl_module \
   --with-http_gzip_static_module \
   --with-http_v2_module \
   --with-http_realip_module \
   --without-http_geo_module \
   --without-http_autoindex_module \
   --without-http_split_clients_module \
   --without-http_memcached_module \
   --without-http_empty_gif_module \
   --without-http_browser_module \
   --add-module=/tmp/nginx-rtmp-module"

ARG PHP_CONF=" \
   --prefix=/usr \
   --libdir=/usr/lib/php \
   --datadir=/usr/share/php \
   --sysconfdir=/php/etc \
   --localstatedir=/php/var \
   --with-pear=/usr/share/php \
   --with-config-file-scan-dir=/php/conf.d \
   --with-config-file-path=/php \
   --with-pic \
   --disable-short-tags \
   --without-readline \
   --enable-bcmath=shared \
   --enable-fpm \
   --disable-cgi \
   --enable-mysqlnd \
   --enable-mbstring \
   --with-curl \
   --with-libedit \
   --with-openssl \
   --with-iconv=/usr/local \
   --enable-gd \
   --with-freetype \
   --with-jpeg \
   --with-webp \
   --disable-gd-jis-conv \
   --with-zlib"

ARG PHP_EXT_LIST=" \
   mysqli \
   ctype \
   curl \
   gd \
   xml \
   mbstring \
   openssl \
   posix \
   session \
   simplexml \
   zip \
   zlib \
   sqlite3 \
   pdo_sqlite \
   pdo_pgsql \
   pdo_mysql \
   fileinfo \
   bz2 \
   intl \
   ldap \
   ftp \
   imap \
   gmp \
   pgsql \
   exif \
   pcntl \
   sodium \
   bcmath \
   "

ARG CUSTOM_BUILD_PKGS=" \
   freetype-dev \
   openldap-dev \
   gmp-dev \
   libmcrypt-dev \
   icu-dev \
   postgresql-dev \
   libpng-dev \
   libwebp-dev \
   gd-dev \
   libjpeg-turbo-dev \
   libxpm-dev \
   libedit-dev \
   libxml2-dev \
   openssl-dev \
   libbz2 \
   libzip-dev \
   oniguruma-dev \
   imap-dev \
   libsodium-dev \
   fftw-dev \
   fontconfig-dev \
   graphviz-dev \
   jbig2dec-dev \
   jbig2enc-dev \
   libde265-dev \
   libheif-dev \
   libjpeg-turbo-dev \
   libpng-dev \
   libpq-dev \
   libraw-dev \
   openjpeg-dev \
   xz-dev \
   zstd-dev \
   lame-dev \
   rtmpdump-dev \
   libtheora-dev \
   libvorbis-dev \
   libvpx-dev \
   x264-dev \
   "

ARG CUSTOM_PKGS=" \
   freetype \
   openldap \
   gmp \
   libmcrypt \
   bzip2-dev \
   icu \
   libpq \
   oniguruma \
   sqlite-dev \
   sqlite \
   c-client \
   libsodium \
   libpng \
   libwebp \
   gd \
   libjpeg-turbo \
   libxpm \
   libedit \
   libxml2 \
   openssl \
   libbz2 \
   libzip \
   oniguruma \
   imap \
   libsodium \
   fftw \
   fontconfig \
   graphviz \
   jbig2dec \
   jbig2enc \
   libde265 \
   x264 \
   libheif \
   libjpeg-turbo \
   libpng \
   libraw \
   openjpeg \
   xz \
   zstd \
   nasm \
   lame \
   rtmpdump \
   "

COPY rootfs /

RUN NB_CORES=${BUILD_CORES-$(getconf _NPROCESSORS_CONF)} \
### Packages installation
   && BUILD_DEPS=" \
      linux-headers \
      libtool \
      build-base \
      pcre-dev \
      zlib-dev \
      wget \
      gnupg \
      autoconf \
      gcc \
      g++ \
      libc-dev \
      make \
      pkgconf \
      curl-dev \
      ca-certificates \
      git \
      ${CUSTOM_BUILD_PKGS}" \
   && apk -U add \
      ${BUILD_DEPS} \
      s6 \
      su-exec \
      curl \
      libedit \
      libxml2 \
      openssl \
      libwebp \
      gd \
      pcre \
      zlib \
      ${CUSTOM_PKGS} \
### Source downloading
   # && wget http://nginx.org/download/nginx-${NGINX_VER}.tar.gz -O /tmp/nginx-${NGINX_VER}.tar.gz \
   # && wget http://nginx.org/download/nginx-${NGINX_VER}.tar.gz.asc -O /tmp/nginx-${NGINX_VER}.tar.gz.asc \
   # && wget ${PHP_MIRROR}/get/php-${PHP_VER}.tar.gz/from/this/mirror -O /tmp/php-${PHP_VER}.tar.gz \
   # && wget ${PHP_MIRROR}/get/php-${PHP_VER}.tar.gz.asc/from/this/mirror -O /tmp/php-${PHP_VER}.tar.gz.asc \
   # && wget http://ftp.gnu.org/pub/gnu/libiconv/libiconv-${LIBICONV_VERSION}.tar.gz -O /tmp/libiconv-${LIBICONV_VERSION}.tar.gz \
   # && wget https://github.com/ImageMagick/ImageMagick/archive/${IMAGEMAGICK_VERSION}.tar.gz -O /tmp/ImageMagick-${IMAGEMAGICK_VERSION}.tar.gz \
   && wget http://ffmpeg.org/releases/ffmpeg-${FFMPEG_VERSION}.tar.gz -O /tmp/ffmpeg-${FFMPEG_VERSION}.tar.gz \
   # && git clone ${RTMP_MODULE_SRC} /tmp/nginx-rtmp-module \
   && git clone https://git.videolan.org/git/ffmpeg/nv-codec-headers.git /tmp/nv-codec-headers \
   && mkdir -p /php/conf.d \
   && mkdir -p /usr/src \
   # && tar xzf /tmp/nginx-${NGINX_VER}.tar.gz -C /usr/src \
   # && tar xzf /tmp/php-${PHP_VER}.tar.gz -C /usr/src \
   # && tar xzf /tmp/libiconv-${LIBICONV_VERSION}.tar.gz -C /usr/src \
   # && tar xzf /tmp/ImageMagick-${IMAGEMAGICK_VERSION}.tar.gz -C /usr/src \
   && tar xzf /tmp/ffmpeg-${FFMPEG_VERSION}.tar.gz -C /usr/src \
### ffmpeg build
   && cd /tmp/nv-codec-headers && make install \
   && echo "Building FFMPEG" \
   && cd /usr/src/ffmpeg-${FFMPEG_VERSION} \
   && ./configure \
      --prefix=/usr \
      --enable-version3 \
      --enable-nonfree \
      --enable-gpl \
      --enable-small \
      --enable-libx264 \
      --enable-libx265 \
      --enable-cuda-nvcc \
      --enable-libnpp \
      --enable-libvpx \
      --enable-libtheora \
      --enable-libvorbis \
      --enable-librtmp \
      --enable-postproc \
      --enable-swresample \
      --enable-libfreetype \
      --enable-libmp3lame \
      --disable-debug \
      --disable-doc \
      --disable-ffplay \
      --extra-libs="-lpthread -lm" \
	&& make -j $(getconf _NPROCESSORS_ONLN) \
	&& make install \
### imagemagick installation
   && echo "Building ImageMagick with HEIC support" \
   && cd /usr/src/ImageMagick-${IMAGEMAGICK_VERSION} \
   && ./configure --with-heic=yes  \
   && make -j ${NB_CORES} && make install \
   && ldconfig /usr/local/lib/ \
### nginx installation
   && cd /usr/src/nginx-${NGINX_VER} \
   && ./configure --with-cc-opt="-O3 -fPIE -fstack-protector-strong" ${NGINX_CONF} \
   && make -j ${NB_CORES} \
   && make install \
### GNU Libiconv installation
   && cd /usr/src/libiconv-${LIBICONV_VERSION} \
   && ./configure --prefix=/usr/local \
   && make -j ${NB_CORES} && make install && libtool --finish /usr/local/lib \
### PHP installation
   && mv /usr/src/php-${PHP_VER} /usr/src/php \
   && cd /usr/src/php \
   && ./configure CFLAGS="-fstack-protector-strong -fpic -fpie -O2 -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64" ${PHP_CONF} \
   && make -j ${NB_CORES} \
   && make install \
### Strip, clean, install modules
   && { find /usr/local/bin /usr/local/sbin -type f -perm +0111 -exec strip --strip-all '{}' + || true; } \
   && make clean \
   && chmod u+x /usr/local/bin/* /etc/s6.d/*/* \
   && chmod u+x /usr/local/bin/docker-php-ext-* \
   && pecl channel-update pecl.php.net \
   && pecl install -f mcrypt \
   && echo "extension=mcrypt.so" > /php/conf.d/mcrypt.ini \
   && pecl install -f imagick \
   && echo "extension=imagick.so" > /php/conf.d/imagick.ini \
   && pecl install -f apcu \
   && echo "extension=apcu.so" > /php/conf.d/apcu.ini \
   && pecl install -f redis \
   && echo "extension=redis.so" > /php/conf.d/redis.ini \
   ## Add Library Path for freetype2 to allow gd to compile successfully
   && export CPATH=/usr/include/freetype2 \
   && docker-php-ext-install ${PHP_EXT_LIST} -j ${NB_CORES} \
### Cleanup
   && apk del ${BUILD_DEPS} \
   && rm -rf /tmp/* /var/cache/apk/* /usr/src/* \
   && mkdir -p /nginx/logs /nginx/run /php/php-fpm.d /php/logs /php/run /php/session